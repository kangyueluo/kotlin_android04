package com.example.kotlin_android04

import android.graphics.Bitmap
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    lateinit var main : View
    lateinit var imageView: ImageView
    lateinit var btn : Button

    val screenShot : ScreenShot = ScreenShot()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        main = findViewById(R.id.main)
        imageView = findViewById(R.id.imageView)
        btn = findViewById(R.id.btn)

        btn.setOnClickListener(View.OnClickListener {
            val b : Bitmap = screenShot.takeRootViewScreenShot(imageView);
            imageView.setImageBitmap(b)

            main.setBackgroundColor(Color.parseColor("#999999"))
        })

    }
}
