package com.example.kotlin_android04

import android.graphics.Bitmap
import android.view.View

class ScreenShot {


    fun takeScreenShot(view: View): Bitmap{

        view.setDrawingCacheEnabled(true)
        view.buildDrawingCache(true)
        val bitmap: Bitmap = Bitmap.createBitmap(view.getDrawingCache())
        view.setDrawingCacheEnabled(false)

        return bitmap

    }


    fun takeRootViewScreenShot(view: View): Bitmap{
        return takeScreenShot(view.rootView)
    }

}